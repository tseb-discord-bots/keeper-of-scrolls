const { Sequelize } = require('sequelize');
const { applyExtraSetup } = require('./extra-setup');
const dbConfig = require('../config/dbConfig.json');

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    logging: dbConfig.logging,
    storage: dbConfig.storage
});

const modelDefiners = [
    require('./models/player.model'),
    require('./models/item.model'),
    require('./models/character.model'),
    require('./models/character_inventory.model'),
    require('./models/note.model'),
    require('./models/tag.model'),
];

for (const modelDefiner of modelDefiners){
    modelDefiner(sequelize);
}

applyExtraSetup(sequelize);

module.exports = sequelize;