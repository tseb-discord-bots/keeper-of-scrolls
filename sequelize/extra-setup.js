function applyExtraSetup(sequelize) {
    const { player, character, item, note, tag } = sequelize.models;
    
    player.hasMany(character);
    character.belongsTo(player);
    
    item.belongsToMany(character, { through: 'character_inventory' });
    character.belongsToMany(item, { through: 'character_inventory' });
    
    player.hasMany(note);
    note.belongsTo(player);

    tag.belongsToMany(item, { through: 'items_tags' });
    item.belongsToMany(tag, { through: 'items_tags' });

    tag.belongsToMany(note, { through: 'players_note_tags' });
    item.belongsToMany(tag, { through: 'players_note_tags' });

}

module.exports = { applyExtraSetup };