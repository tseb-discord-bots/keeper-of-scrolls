const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('note', {
        name:{
            type:DataTypes.STRING,
            allowNull: false,
        },
        note:{
            type:DataTypes.STRING,
            allowNull: false,
        }
    },
    {
        timestamps:false,
    });
};