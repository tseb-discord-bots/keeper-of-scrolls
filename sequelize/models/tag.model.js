const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('tag', {
        tag:{
            type:DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        timestamps:false,
    });
};