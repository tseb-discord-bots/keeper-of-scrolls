const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('item', {
        name:{
            type:DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        description:{
            type:DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        timestamps:false,
    });
};