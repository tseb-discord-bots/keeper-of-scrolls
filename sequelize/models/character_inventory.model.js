const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('character_inventory', {
        amount:{
            type:DataTypes.INTEGER,
            allowNull: false,
            'default':0,
        },
    },
    {
        timestamps:false,
    });
};