const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('player', {
        discord_id:{
            type:DataTypes.STRING,
            unique:true,
            allowNull: false,
        },
        player_name:{
            type:DataTypes.STRING,
            allowNull:false,
        }
    },
    {
        timestamps:false,
    });
};