const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    return sequelize.define('character', {
        name:{
            type:DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        timestamps:false,
    });
};