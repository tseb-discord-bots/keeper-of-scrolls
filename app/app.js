const Discord = require('discord.js');
const config = require('../config/config.json');
const fs = require('fs');
const { models } = require('../sequelize/index');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./app/commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
};


client.on('message', async function(message) { 
    var prefix = '';
    
    const PREFIX_EMOJI = client.emojis.cache.find(emoji => emoji.name === config.emoji_name);
    
    if (message.author.bot) return; // prevent responses to other bots
    
    if (message.content.toLowerCase().startsWith(config.prefix)) prefix = config.prefix;
    
    if(PREFIX_EMOJI && message.content.startsWith(`<:gmh:${PREFIX_EMOJI.id}>`)) prefix = `<:gmh:${PREFIX_EMOJI.id}>`;

    if(prefix === '') { return; };

    const commandBody = message.content.slice(prefix.length);
    const args= commandBody.trimStart().split(/ +/).filter(x => x != '').map(y => y.toLowerCase());
    const commandName = args.shift();
    const argString = args.join(' ');

    if(commandName === 'help'){
        let response = client.commands.map(x =>
            `:gmh: ${x.name} \n\t\tDescription: ${x.description} \n\t\tCommand Arguments: ${x.args}`
        );
        message.channel.send(response);
        return;
    }

    if(!client.commands.has(commandName)) return;

    const command = client.commands.get(commandName);

    if(!command){
        message.reply(`I'm so very sorry. I don't recognize that command. Try using the 'help' command`);
    }

    try{
        if(command.is_async){
            await command.execute(message, argString, models);
        } else {
            command.execute(message, argString, models);
        }
    } catch (error) {
        console.error(error);
        message.reply(`I'm so very sorry there was an error while I tried to execute that command.`);
    }
});           


client.login(config.BOT_TOKEN);