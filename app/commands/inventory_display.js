const { model } = require("../../sequelize");

module.exports = {
    name:"display_inventory",
    description: "show the characters inventory",
    is_async: true,
    args:'character name',
    async execute(message, argString, models){
        let character = await models.character.findOne({
            where: {
                name: argString
            }
        });
        let inventoryItems = await character.getItems();
        let response = '';
        if(inventoryItems.length === 0){
            response = `${character.name} has no items.`
        } else {
        response = inventoryItems.map( x =>
                `${x.name} | ${x.description} | ${x.character_inventory.amount}\n`
            );
        }
        message.channel.send(`${character.name}'s inventory \n${response}`);
    },
};