const { model } = require("../../sequelize");

module.exports = {
    name:"search_notes",
    description: "search for note if '|show' is added at the end the note will be posted in the channel otherwise it will be DMd",
    is_async: true,
    args:'note name|show(optional)',
    async execute(message, argString, models){
        const args = argString.split('|');
        const note = await models.note.findOne({
            where:{
                name: args[0]
            }
        });
        if(args.length > 1 && args[1] === 'show'){
            message.channel.send(`${note.name}:\n${note.note}`);
        } else {
            message.author.send(`${note.name}:\n${note.note}`);
        }
    },
};