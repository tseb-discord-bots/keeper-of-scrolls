const { model } = require("../../sequelize");

module.exports = {
    name:"pin_note",
    description: "pins the note to the channel",
    is_async: true,
    args:'note name',
    async execute(message, argString, models){
        const note = await models.note.findOne({
            where:{
                name: argString
            }
        });
        const reply = await message.channel.send(`${note.name}:\n${note.note}`);
        reply.pin();
    },
};