const { model } = require("../../sequelize");

module.exports = {
    name:"add_to_inventory",
    description: "add the listed item to a character inventory, default amount is 1 if skipped",
    is_async: true,
    args:'character name|item name|amount(optional)',
    async execute(message, argString, models){
        let args = argString.split('|')
        let itemAmount =  args.length > 2 ? parseInt(args[2]) : 1;
        let character = await models.character.findOne({
            where: {
                name: args[0]
            }
        });
        let item = await models.item.findOne({
            where: {
                name: args[1]
            }
        })
        if(character && item){
            let inventoryItem = await models.character_inventory.findOne({
                where:{
                    characterId:character.id,
                    itemId: item.id
                }
            })
            if(inventoryItem){
                await inventoryItem.increment('amount', {by: itemAmount});
            } else {
                await character.addItem(item, {through: { amount: itemAmount}});
            }
            message.channel.send(`${args[0]} had ${args[1]} added to their inventory`)
        }
        else{
            if(!character) {
                message.channel.send(`I can't find character: ${args[0]}`);
            }
            if(!item) {
                message.channel.send(`I can't find item: ${args[1]}`);
            }
        }
    },
};