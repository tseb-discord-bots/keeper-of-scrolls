module.exports = {
    name:"display_item",
    description: "display an item",
    is_async: true,
    args:'item name',
    async execute(message, argString, models){
        const itemResult = await models.item.findOne({
            where:{name: argString}
        });
        if(itemResult){
            return message.channel.send(`${itemResult.name} | ${itemResult.description}`);
        } else {
            return message.channel.send(`I can't find that item.`);
        }
    },
};