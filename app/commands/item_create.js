module.exports = {
    name:"create_item",
    description: "create a new item, if level is left off it is set to 0",
    is_async: true,
    args:'item name|item description',
    async execute(message, argString, models){
        const itemArgs = argString.split('|');
        const itemName = itemArgs[0];
        const itemDescription = itemArgs[1];
        const itemLevel = itemArgs.length > 2 ? parseInt(itemArgs[2]) : 0;
        const itemResult = await models.item.findOrCreate({
            where:{name: itemName},
            defaults:{name: itemName, description: itemDescription}
        });

        return message.channel.send(`${itemResult[0].name} has been created.`);
    },
};