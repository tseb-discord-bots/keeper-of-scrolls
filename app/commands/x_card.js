module.exports = {
    name:'x_card',
    description: 'let someone know that an x-card has been called.',
    is_async: true,
    args:'@UsernNameToSendXCardTo',
    async execute(message, argString, models) {
        const target = message.mentions.users.first();
        return target.send(`An x-card has been sent to you.`);
    }
};