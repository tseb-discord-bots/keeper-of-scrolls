module.exports = {
    name:'create_character',
    description: 'create a character for the tagged user or the author of the message',
    is_async: true,
    args:'@UserName(optional) character name',
    async execute(message, argString, models) {
        args = argString.split(' ');
        let characterName = '';
        let owningPlayer;
        const target = message.mentions.users.first();

        if(target){
            owningPlayer = target
            args.shift()
        } else{
            owningPlayer = message.author;
        }
        characterName = args.join(' ');

        const player = await models.player.findOrCreate({
            where:{discord_id: owningPlayer.id},
            defaults:{discord_id: owningPlayer.id, player_name: owningPlayer.username}
        })

        const character = await models.character.findOrCreate({
            where:{name: characterName},
            defaults:{playerId: player[0].id, name: characterName}
        });

        return message.channel.send(`${character[0].name} has been created for <@${player[0].discord_id}>`);
    }
};