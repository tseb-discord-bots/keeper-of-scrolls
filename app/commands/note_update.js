const { model } = require("../../sequelize");

module.exports = {
    name:"update_note",
    description: "update the note to have a new description",
    is_async: true,
    args:'note name|new description',
    async execute(message, argString, models){
        const noteArgs = argString.split('|');
        const noteName = noteArgs[0];
        const noteNewDescription = noteArgs[1];
        const noteTags = noteArgs.length > 2 ? noteArgs[2].split(','): [''];
        const noteResult = await models.note.findOrCreate({
            where:{name: noteName},
            defaults:{name: noteName, note: noteNewDescription}
        });
        noteResult[0].note = noteNewDescription;
        await noteResult[0].save();
        return message.channel.send(`${noteResult[0].name} has been updated.`);

    },
};