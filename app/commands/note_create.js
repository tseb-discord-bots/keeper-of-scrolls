const { model } = require("../../sequelize");

module.exports = {
    name:"create_note",
    description: "create a note",
    is_async: true,
    args:'note name|note description',
    async execute(message, argString, models){
        const noteArgs = argString.split('|');
        const noteName = noteArgs[0];
        const noteDescription = noteArgs[1];
        const noteTags = noteArgs.length > 2 ? noteArgs[2].split(','): [''];
        const noteResult = await models.note.findOrCreate({
            where:{name: noteName},
            defaults:{name: noteName, note: noteDescription}
        });

        return message.channel.send(`${noteResult[0].name} has been created.`);
    },
};