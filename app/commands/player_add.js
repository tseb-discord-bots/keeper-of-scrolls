module.exports = {
    name:'add_player',
    description: 'add a player to a campaign',
    is_async: true,
    args:'@UserNameToAdd',
    async execute(message, argString, models) {
        const target = message.mentions.users.first();

        const newPlayer = await models.player.findOrCreate({
            where:{discord_id: target.id},
            defaults:{discord_id: target.id, player_name: target.username}
        });

        return message.channel.send(`<@${newPlayer[0].discord_id}> has been added.`);
    }
};