const sequelize = require('./sequelize');

const force = process.argv.includes('--force') || process.argv.includes('-f');

sequelize.sync({ force }).then(async () => {
    const setup = [
    ];
    await Promise.all(setup);
    console.log('Database synced');
    sequelize.close();
}).catch(console.error);